(use-modules ((gnunet sync)))


(define (run-search)
  "Run a background thread to look for new messages"
  (call-with-new-thread
   (lambda ()
     (pk "starting thread")
     (let loop ()
       (search "etc/p2.conf" '("c3b2://v1/message"))
       (loop)
       (sleep 1)))))

(run-search)
(run-search)
