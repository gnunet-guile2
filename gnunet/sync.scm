;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;;
;;;; Copyright © 2018 Amirouche Boubekki <amirouche@hypermove.net>
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(define-module (gnunet sync))

(use-modules ((ice-9 binary-ports)))
(use-modules ((ice-9 threads)))
(use-modules ((srfi srfi-1)))

(use-modules ((gnunet)))


;;; publish

(define (publish/task/shutdown publish-context)
  (lambda ()
    (fs-publish-stop publish-context)))

(define (publish/progress callback)
  (lambda (info)
    (let ((status (fs-progress-info-status info)))
      (cond
       ((eq? status %fs-status-publish-completed)
        (callback (uri->string (fs-progress-info-publish-chk-uri info)))
        (scheduler-shutdown))))))

(define (publish/task configuration filename callback)
  (lambda ()
    (let ((fs (fs-start configuration "c3b2" (publish/progress callback))))
      (let ((fi (fs-file-information-create-from-file
                 fs
                 filename
                  ;; TODO: doesn't work, see publish-keywords
                 (fs-uri-ksk-create "c3b2://v1/message")
                 (container-meta-data-create)
                 %gnunet-yes
                 (fs-block-options))))
        (let ((publish-context (fs-publish-start fs fi)))
          (scheduler-add-shutdown (publish/task/shutdown publish-context)))))))

(define (publish-exec configuration filename callback)
  (let ((configuration* (configuration-create)))
    (configuration-load! configuration* configuration)
    (scheduler-run (publish/task configuration* filename callback))))

(define (publish-file configuration filename)
  (let ((out #f))
    (let ((thread (call-with-new-thread
                   (lambda ()
                     (publish-exec configuration
                                   filename
                                   (lambda (uri) (set! out uri)))))))
      (join-thread thread))
    out))

(define (invoke program . args)  ;; stolen from guix
  "Invoke PROGRAM with the given ARGS.  Raise an error if the exit
code is non-zero; otherwise return #t."
  (let ((status (apply system* program args)))
    (unless (zero? status)
      (error (format #f "program ~s exited with non-zero code" program)
             status))
    #t))

(define (publish-keywords configuration uri keywords)
  (apply invoke (append (list "gnunet-publish" "-c" configuration "-u" uri)
                        (append-map (lambda (keyword) (list "-k" keyword))
                                    keywords))))

(define-public (publish configuration filename keywords)
  "Publish FILENAME tagged with keywords"
  (pk 1 configuration filename keywords)
  (let ((uri (publish-file configuration filename)))
    (pk 2)
    (publish-keywords configuration uri keywords)
    (pk 3)
    uri))

;;; search

(define (search/task/shutdown search-context)
  (lambda ()
    (fs-search-stop search-context)))

(define (search/progress callback)
  (lambda (info)
    (let ((status (fs-progress-info-status info)))
      (cond
       ((eq? status %fs-status-search-result)
        (callback (uri->string (fs-progress-info-search-result-uri info))))))))

(define (search/timeout)
  (scheduler-shutdown))

(define %five-seconds (* 5 (expt 10 6)))

(define (search/task configuration uri callback)
  (lambda ()
    (let ((fs (fs-start configuration "c3b2" (search/progress callback))))
      (let ((search-context (fs-search-start fs uri 1 %fs-search-option-none)))
        (scheduler-add-delayed %five-seconds search/timeout)
        (scheduler-add-shutdown (search/task/shutdown search-context))))))

(define (search-exec configuration keywords callback)
  (let ((configuration* (configuration-create))
        (uri (apply fs-uri-ksk-create keywords)))
    (configuration-load! configuration* configuration)
    (scheduler-run (search/task configuration* uri callback))))

(define-public (search configuration keywords)
  "Search for files on matchin KEYWORDS"
  (let ((out '()))
    (let ((thread (call-with-new-thread
                   (lambda ()
                     (search-exec configuration
                                  keywords
                                  (lambda (uri) (set! out (cons uri out))))))))
      (join-thread thread))
    out))

;;; download

(define (download/task/clean-up fs)
  (lambda ()
    (fs-stop fs)))

(define (download/progress callback)
  (lambda (info)
    (let ((status (fs-progress-info-status info)))
      (cond
       ((eq? status %fs-status-download-completed)
        (callback)
        (scheduler-shutdown))
       ((eq? status %fs-status-download-stopped)
        (scheduler-add-now (download/task/clean-up (fs-progress-info-fs info))))))))


(define (download/task/shutdown download-context)
  (lambda ()
    (fs-download-stop download-context #true)))

(define (download/timeout)
  (scheduler-shutdown))

(define %one-second (* 1 (expt 10 6)))

(define (download/task configuration uri callback)
  (lambda ()
    (let ((fs (fs-start configuration "c3b2" (download/progress callback))))
      (let ((download-context (fs-download-start fs uri 0 #:filename "c3b2.out")))
        (scheduler-add-delayed %one-second download/timeout)
        (scheduler-add-shutdown (download/task/shutdown download-context))))))

(define (download-exec configuration uri callback)
  (let ((configuration* (configuration-create)))
    (configuration-load! configuration* configuration)
    (let ((uri (string->uri uri)))
      (scheduler-run (download/task configuration* uri callback)))))

(define-public (download configuration uri)
  "Download URI and return its bytevector"
  (let ((out #f))
    (let ((thread (call-with-new-thread
                   (lambda ()
                     (download-exec configuration
                                    uri
                                    (lambda ()
                                      (call-with-input-file "c3b2.out"
                                        (lambda (port)
                                          (set! out (get-bytevector-all port)))
                                        #:binary #t)))))))
      (join-thread thread))
    out))
