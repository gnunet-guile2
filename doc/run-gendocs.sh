#!/bin/sh

make version.texi
./gendocs.sh --email gnunet-developers@gnu.org gnunet-guile "GNUnet Guile Reference Manual" -o "manual/gnunet-guile"
cp "index.html" manual/
printf "Success"
