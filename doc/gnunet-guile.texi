\input texinfo
@c -*-texinfo-*-

@c %**start of header
@setfilename gnunet-guile.info
@documentencoding UTF-8
@settitle GNUnet Guile Reference Manual
@exampleindent 2
@urefbreakstyle before
@c %**end of header

@include version.texi

@copying
Copyright @copyright{} 2017 GNUnet e.V.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
copy of the license is included in the section entitled ``GNU Free
Documentation License''.

A copy of the license is also available from the Free Software
Foundation Web site at @url{http://www.gnu.org/licenses/fdl.html}.

Alternately, this document is also available under the General
Public License, version 3 or later, as published by the Free Software
Foundation.  A copy of the license is included in the section entitled
``GNU General Public License''.

A copy of the license is also available from the Free Software
Foundation Web site at @url{http://www.gnu.org/licenses/gpl.html}.
@end copying

@dircategory Networking
@direntry
* GNUnet-Guile: (gnunet-guile).       Guile bindings for GNUnet
@end direntry

@titlepage
@title GNUnet Guile Reference Manual
@subtitle Guile bindings for GNUnet
@author The GNUnet Developers

@page
@vskip 0pt plus 1filll
Edition @value{EDITION} @*
@value{UPDATED} @*

@insertcopying
@end titlepage

@summarycontents
@contents

@node Top
@top Contributing to GNUnet


This document describes GNUnet Guile version @value{VERSION}.


GNUnet Guile is a maintained as part of the
@uref{https://gnunet.org/, GNUnet} project.

All code contributions must thus be put under the
@uref{https://www.gnu.org/copyleft/gpl.html, GNU Public License (GPL)}.
All documentation should be put under FSF approved licenses
(see @uref{https://www.gnu.org/copyleft/fdl.html, fdl}).

By submitting documentation, translations, comments and other
content to this project you automatically grant the right to publish
code under the GNU Public License (version 3 or later) and
documentation under either or both the GNU Public License or the
GNU Free Documentation License.
When contributing to the GNUnet Guile project, GNU standards and the
@uref{https://www.gnu.org/philosophy/philosophy.html, GNU philosophy}
should be adhered to.

Unlike GNUnet, we require no formal copyright assignments
for contributions to GNUnet Guile.

@menu

* Introduction::                    Introduction
* Main::                            Main
* GNU Free Documentation License::  The license of this manual.
* GNU General Public License::      The license of this manual.
* Concept Index::                   Concepts.
* Programming Index::               Data types, functions, and variables.

@detailmenu
 --- The Detailed Node Listing ---

Introduction

Main

@end detailmenu
@end menu

@node Introduction
@chapter Introduction

@node Main
@chapter Main

@c *********************************************************************
@node GNU Free Documentation License
@appendix GNU Free Documentation License
@cindex license, GNU Free Documentation License
@include fdl-1.3.texi

@c *********************************************************************
@node GNU General Public License
@appendix GNU General Public License
@cindex license, GNU General Public License
@include gpl-3.0.texi

@c *********************************************************************
@node Concept Index
@unnumbered Concept Index
@printindex cp

@node Programming Index
@unnumbered Programming Index
@syncodeindex tp fn
@syncodeindex vr fn
@printindex fn

@bye
