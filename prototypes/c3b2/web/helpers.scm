;;; Copyright © 2015-2017  Amirouche Boubekki <amirouche@hypermove.net>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(define-module (web helpers))

;; stdlib
(use-modules (web request))
(use-modules (web response))
(use-modules (web uri))

;; local
(use-modules (web html))


;;; helpers

(define-public (request-path-components request)
  "Split the URI path of REQUEST into a list of component strings.  For
example: \"/foo/bar\" yields '(\"foo\" \"bar\")."
  (split-and-decode-uri-path (uri-path (request-uri request))))

(define-public (render-html sxml)
  (values '((content-type . (text/html)))
          (lambda (port)
            (sxml->html sxml port))))

(define-public (forbidden)
  (values (build-response #:code 403)
          "Forbidden"))

(define-public (not-found uri)
  (values (build-response #:code 404)
          (string-append "Resource not found: " uri)))

(define-public (redirect uri)
  (values (build-response #:code 303 #:headers `((Location . ,uri))) ""))

(define-public (error)
  (values (build-response #:code 500)))
